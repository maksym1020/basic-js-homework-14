'use strict'
/* Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/

const changeThemeButton = document.createElement('button');
changeThemeButton.innerText = 'Change theme';
changeThemeButton.classList.add('change-theme-button');

const cart = document.querySelector('.cart');
cart.before(changeThemeButton);


const header = document.querySelector('.header-menu-top-blue');
const main = document.querySelector('.main');

if (localStorage.getItem('alternativeTheme')) {
    main.classList.toggle('main-alternative');
    header.classList.toggle('header-menu-top-alternative');
}

changeThemeButton.addEventListener('click', (e)=> {
    main.classList.toggle('main-alternative');
    header.classList.toggle('header-menu-top-alternative');

    if(localStorage.getItem('alternativeTheme')){
        localStorage.removeItem('alternativeTheme')
    } else {
        localStorage.setItem('alternativeTheme', 'true');
    }

})